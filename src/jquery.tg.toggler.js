// Uses CommonJS, AMD or browser globals to create a jQuery plugin.
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof module === 'object' && module.exports) {
		module.exports = function (root, jQuery) {
			if (jQuery === undefined) {
				if (typeof window !== 'undefined') {
					jQuery = require('jquery');
				} else {
					jQuery = require('jquery')(root);
				}
			}
			factory(jQuery);
			return jQuery;
		};
	} else {
		factory(jQuery);
	}
}(function ($) {
	var pluginName = 'tgToggler';
	var namespace = 'tgToggler';
	var defaults = {
		$targets: false,
		$handles: false,
		openClass: 'open',
		openOnInit: false,
		closeAllOnEsc: false,
		closeAllOnBodyClick: false,
		allowMultiOpen: false,
		allowSelfClose: true,
	};

	function TGToggler(element, options) {
		this.opts = $.extend({}, defaults, options);
		this._defaults = defaults;
		this._name = pluginName;

		this.el = element;
		this.$el = $(element);
		this.$handles = this.opts.$handles;
		this.$targets = this.opts.$targets;

		this.init();
	}

	TGToggler.prototype = {

		init: function () {
			var self = this;
			var $initTarget;
			var $initHandle;

			// Setup handlers to open/close/toggle each element
			if (self.$handles) {
				self.$handles.on('click', function (event) {
					var $handle = $(event.target);
					var $target = $($handle.data('target'));
					if ($target.length) {
						// Execute the appropriate action
						if ($handle.data('action') === 'open') {
							self.show($target, $handle);
						} else if ($handle.data('action') === 'close') {
							self.hide($target, $handle);
						} else {
							self.toggle($target, $handle);
						}
						event.preventDefault();
						event.stopPropagation();
					}
				});
			}

			// Open the the elements on init
			if (self.opts.openOnInit) {
				$initTarget = self.opts.openOnInit[0];
				$initHandle = self.opts.openOnInit[1];
				self.show($initTarget, $initHandle);
			}

			// ESC handler
			if (this.opts.closeAllOnEsc) {
				window.addEventListener('keydown', function (event) {
					if (event.keyCode === 27) {
						self.closeAll(true);
					}
				});
			}

			// Body click handler
			if (this.opts.closeAllOnBodyClick) {
				$('body').on('click', function () {
					self.closeAll(true);
				});
			}

			return self;
		},

		// Show the $target
		// isToggling indicates that the method was called internally by toggle()
		show: function ($target, $handle, isToggling) {
			var self = this;
			var eventArgs = [self, $target, $handle, isToggling];
			self.$el.trigger('show.' + namespace, eventArgs);
			if (!self.opts.allowMultiOpen) {
				self.closeAll(!!$handle);
			}
			$target.addClass(self.opts.openClass);
			if ($handle) {
				$handle.addClass(self.opts.openClass);
			}
			self.$el.trigger('shown.' + namespace, eventArgs);
			return self;
		},

		// Hide the $target
		// isToggling indicates that the method was called internally by toggle()
		hide: function ($target, $handle, isToggling) {
			var self = this;
			var eventArgs = [self, $target, isToggling];
			self.$el.trigger('hide.' + namespace, eventArgs);
			$target.removeClass(self.opts.openClass);
			if ($handle) {
				$handle.removeClass(self.opts.openClass);
			}
			self.$el.trigger('hidden.' + namespace, eventArgs);
			return self;
		},

		// Open/close the $target
		toggle: function ($target, $handle) {
			var self = this;
			if ($target.hasClass(self.opts.openClass)) {
				if (self.opts.allowSelfClose) {
					self.hide($target, $handle, true);
				}
			} else {
				self.show($target, $handle, true);
			}
			return self;
		},

		// Close all open targets
		// closeHandles determines if handles should be manually closed too
		closeAll: function (closeHandles) {
			var self = this;
			self.$targets.filter('.' + self.opts.openClass).each(function (i, target) {
				self.hide($(target));
			});
			if (closeHandles) {
				self.$handles.removeClass(self.opts.openClass);
			}
			return self;
		},
	};

	// Add the plugin to the jQuery namespace
	$.fn[pluginName] = function (options) {
		return this.each(function () {
			if (!$.data(this, pluginName)) {
				$.data(this, pluginName, new TGToggler(this, options));
			}
		});
	};
}));
