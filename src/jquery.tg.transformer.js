// Uses CommonJS, AMD or browser globals to create a jQuery plugin.
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof module === 'object' && module.exports) {
		module.exports = function (root, jQuery) {
			if (jQuery === undefined) {
				if (typeof window !== 'undefined') {
					jQuery = require('jquery');
				} else {
					jQuery = require('jquery')(root);
				}
			}
			factory(jQuery);
			return jQuery;
		};
	} else {
		factory(jQuery);
	}
}(function ($) {
	var pluginName = 'tgTransformer';

	function TGTransformer(element, options) {
		this.transforms = options || [];

		this.el = element;
		this.$el = $(element);

		this.init();
	}

	TGTransformer.prototype = {

		// Set up handlers to run the transform callbacks
		init: function () {
			var self = this;
			$.each(self.transforms, function (i, transform) {
				var mq = window.matchMedia('(' + transform.mq + ')');
				mq.addListener(function () {
					transform.callback(self.$el, mq);
				});
				if (transform.runOnInit) {
					transform.callback(self.$el, mq);
				}
			});
			return self;
		},
	};

	// Add the plugin to the jQuery namespace
	$.fn[pluginName] = function (options) {
		return this.each(function () {
			if (!$.data(this, pluginName)) {
				$.data(this, pluginName, new TGTransformer(this, options));
			}
		});
	};
}));
