// Uses CommonJS, AMD or browser globals to create a jQuery plugin.
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof module === 'object' && module.exports) {
		module.exports = function (root, jQuery) {
			if (jQuery === undefined) {
				if (typeof window !== 'undefined') {
					jQuery = require('jquery');
				} else {
					jQuery = require('jquery')(root);
				}
			}
			factory(jQuery);
			return jQuery;
		};
	} else {
		factory(jQuery);
	}
}(function ($) {
	var pluginName = 'tgExpander';
	var defaults = {
		$targets: false,
		$expanders: false,
		expandedClass: 'expanded',
		expandString: 'Show more',
		collapseString: 'Show less',
		collapseOnHide: true,
	};

	function TGExpander(element, options) {
		this.opts = $.extend({}, defaults, options);
		this._defaults = defaults;
		this._name = pluginName;

		this.el = element;
		this.$el = $(element);
		this.$expanders = this.opts.$expanders;
		this.$targets = this.opts.$targets;

		this.init();
	}

	TGExpander.prototype = {

		init: function () {
			var self = this;

			// Setup handlers to expand/collapse each element
			if (self.$expanders) {
				self.$expanders.on('click', function (event) {
					var $expander = $(event.target);
					var $target = $($expander.data('target'));
					self.toggle($target, $expander);
					event.preventDefault();
				});
			}

			// Collapse expanded elements when the target is hidden
			if (self.opts.collapseOnHide) {
				self.$el.on('hidden.tgToggler', function (event, toggler, $target) {
					var id = $target[0].id;
					var expander = self.$expanders.filter('[data-target="#' + id + '"]');
					self.collapse($target, $(expander));
				});
			}

			return self;
		},

		// Expande the content
		expand: function ($target, $expander) {
			var self = this;
			$target.addClass(self.opts.expandedClass);
			if ($expander) {
				$expander.addClass(self.opts.expandedClass);
				$expander.text(self.opts.collapseString);
			}
			return self;
		},

		// Collapse the content
		collapse: function ($target, $expander) {
			var self = this;
			$target.removeClass(self.opts.expandedClass);
			if ($expander) {
				$expander.removeClass(self.opts.expandedClass);
				$expander.text(self.opts.expandString);
			}
			return self;
		},

		// Toggle the expanded/collapsed state
		toggle: function ($target, $expander) {
			var self = this;
			if ($target.hasClass(self.opts.expandedClass)) {
				self.collapse($target, $expander);
			} else {
				self.expand($target, $expander);
			}
			return self;
		},
	};

	// Add the plugin to the jQuery namespace
	$.fn[pluginName] = function (options) {
		return this.each(function () {
			if (!$.data(this, pluginName)) {
				$.data(this, pluginName, new TGExpander(this, options));
			}
		});
	};
}));
