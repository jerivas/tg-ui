# TG UI

Lightweight jQuery components. All plugins only swap classes around and do some
minor DOM manipulation. You should handle animations, sizing, and positioning
in CSS.

## TG Toggler

Add open/close/toggle capabilities to any element. It can be used to create
accordions, tabs, dropdown menus, sliding panels, and more, because it simply
swaps classes in and out. How the elements look and are animated depends on the
CSS that targets said classes.

```javascript
$('.element').tgToggler({
	$targets: $('.targets'),
	$handles: $('.handles'),
	openClass: 'open',
	openOnInit: [$('.first-target'), $('.first-handle')],
	closeAllOnEsc: false,
	closeAllOnBodyClick: false,
	allowMultiOpen: false,
	allowSelfClose: true,
});
```

### Available options

- `$targets`: (required) jQuery object with all nodes that will be toggled.
- `$handles`: (required if you want click handling) jQuery object with all
  nodes that will trigger the toggler. Each toggle must have a `data-
  target="#id"` attribute with a unique selector for the element it wants to
  open/close. It can also have an optional `data-action="open|close|toggle"` to
  indicate the action that it will perform on it's target. If this is not
  found, the `toggle` action is executed.
- `openClass`: (optional, default `'open'`) Class string that will be given to
  both the toggle and the target when they are open (shown). You should hook
  your CSS to this.
- `openOnInit`: (optional) Array of jQuery objects, where the first is a member
  of `targets` and the second (optional) is a member of `handles`. These two
  elements will be opened when the toggler initializes if they are defined.
- `closeAllOnEsc`: (optional, default `false`) Close all targets when hitting
  the ESC key.
- `closeAllOnBodyClick`: (optional, default `false`) Close all targets when
  clicking on the document's `<body>`.
- `allowMultiOpen`: (optional, default `false`) Allow multiple targets to be
  open at the same time. Otherwise, close all targets before opening a new one.
- `allowSelfClose`: (optional, default `true`) When toggling, allow targets to
  close by clicking on them a second time. Otherwise, the only way to close a
  target is to open another one.

### Events

You can hook event handlers for the show/hide actions. The event handler should
have the following signature:

```javascript
function handler (event, toggler, $target, $handle, isToggling) {
	// event: the event object passed by jQuery
	// toggler: the tgToggler instance, the jQuery element is toggler.$el
	// $target: the jQuery object for the target of $toggle
	// $handle: the jQuery object for the toggle that triggered the event (will be undefined if the event was fired programatically).
	// isToggling: will be true if the event was not triggered by a direct show/hide, but rather by the toggle() method.
}
```

The available events are:

- `show.tgToggler`: Think onBeforeShow
- `shown.tgToggler`: Think onAfterShow
- `hide.tgToggler`: Think onBeforeHide
- `hidden.tgToggler`: Think onAfterHide

## TG Expander

Useful for showing teaser/full text snippets.

```javascript
$('.element').tgExpander({
	$targets: false,
	$expanders: false,
	expandedClass: 'expanded',
	expandString: 'Show more',
	collapseString: 'Show less',
	collapseOnHide: true,
});
```

### Available options

- `$targets`: (optional) jQuery object with all nodes that will be
  expanded/collapsed.
- `$handles`: (required) jQuery object with all nodes that will trigger the
  expander. Each toggle must have a `data- target="#id"` attribute with a
  unique selector for the element it wants to open/close.
- `expandedClass`: Class string that will be given to both the expander and the
  target when they are expanded. You should hook your CSS to this.
- `expandString`: Text content of the expander when collapsed.
- `collapseString`: Text content of the expander when expanded.
- `collapseOnHide`: Attach an event handler listening to `hidden.tgToggler` so
  the element collapses after being closed by tgToggle.

## TG Transformer

Execute arbitrary functions when media queries are triggered.

```javascript
$('.element').tgTransformer([
	{
		mq: 'min-width: 992px',
		callback: function ($element, mq) {},
		runOnInit: false,
	},
]);
```

### Available options

Please note that in this case the options are an array of objects, instead of a
single object. This allows you to define as many transforms as you like for any
number of media queries.

- `mq`: The media query string that will trigger the transformation.
- `callback`: Function that will be executed whenever the media query threshold
  is passed. Inside the function you can check if the media query is currently
  active or not with `mq.matches`. You also have access to the jQuery object
  for `.element` as `$element`.
- `runOnInit`: Specify if `callback` should be executed when the plugin is
  initialized. If `false`, it will simply be bound to the media query and be
  executed until a threshold pass occurs.

## Full example: A tabccordion

A tabccordion behaves as an accordion in mobile viewports, and as tabs in
desktops. It also supports expanding/collapsing the inner contents of the tab
body, to avoid having super tall accordions or tabs.

```html
<div class="tabccordion">
  <div class="tab-header" data-target="#tab-1">Lorem ipsum.</div>
  <div class="tab-body" id="tab-1">
      Lorem ipsum dolor sit.
      <button class="expand-toggle" data-target="#tab-1">Show more</button>
  </div>
  <div class="tab-header" data-target="#tab-2">Nesciunt, ad?</div>
  <div class="tab-body" id="tab-2">
      Omnis quis nam, in!
      <button class="expand-toggle" data-target="#tab-2">Show more</button>
  </div>
</div>
```

```javascript
// A generic function for transforming accordions into tabs and vice versa
var transformTabccordion = function ($el, mq) {
	var bodyContainer;
	var headerContainer;
	var $tabHeaders = $($el.find('.tab-header'));
	var $tabBodies = $($el.find('.tab-body'));

	// Add the containers for tab mode if not there
	if (!$el.find('.tab-body-container').length) {
		$('<div class="tab-body-container" />').prependTo($el);
	}
	if (!$el.find('.tab-header-container').length) {
		$('<div class="tab-header-container" />').prependTo($el);
	}

	headerContainer = $el.find('.tab-header-container');
	bodyContainer = $el.find('.tab-body-container');

	// Check if the media query matches
	if (mq.matches) {
		// Transform into tabs. Move headers and bodies into their containers
		$tabHeaders.appendTo($(headerContainer));
		$tabBodies.appendTo($(bodyContainer));
		$el.removeClass('is-accordion').addClass('is-tabs');
	} else {
		// Transform into accordion. Move headers and bodies out of their containers
		// intertwining them
		$tabHeaders.each(function (i, tabHeader) {
			$(tabHeader).appendTo($el);
			$($tabBodies[i]).appendTo($el);
		});
		$el.addClass('is-accordion').removeClass('is-tabs');
	}
};

var $handles = $('.tabccordion .tab-header');
var $targets = $('.tabccordion .tab-body');
$('.tabccordion')
	.tgToggler({
		$targets: $targets,
		$handles: $handles,
		openOnInit: [
			$targets.first(),
			$handles.first(),
		],
	})
	.tgExpander({
		$expanders: $($targets.find('.expand-toggle')),
	})
	.tgTransformer([
		{
			mq: 'min-width: 992px',
			callback: window.transformTabccordion,
			runOnInit: true,
		}
	]);
```
